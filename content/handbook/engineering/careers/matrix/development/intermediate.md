---
title: "Development Department Career Framework: Intermediate"
---

## Development Department Competencies: Intermediate

{{% include "includes/engineering/dev-career-matrix-nav.md" %}}

**Intermediates at GitLab are expected to exhibit the following competencies:**

- [Intermediate Leadership Competencies](#intermediate-leadership-competencies)
- [Intermediate Technical Competencies](#intermediate-technical-competencies)
- [Intermediate Values Alignment](#intermediate-values-alignment)

---

### Intermediate Leadership Competencies

{{% include "includes/engineering/intermediate-leadership-competency.md" %}}
{{% include "includes/engineering/development-intermediate-leadership-competency.md" %}}
  
### Intermediate Technical Competencies

{{% include "includes/engineering/intermediate-technical-competency.md" %}}
{{% include "includes/engineering/development-intermediate-technical-competency.md" %}}

### Intermediate Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
{{% include "includes/engineering/development-intermediate-values-competency.md" %}}
